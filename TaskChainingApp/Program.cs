﻿using System;
using System.Threading.Tasks;

namespace TaskContinuationApp
{
    internal class Program
    {
        static async Task Main()
        {
            var taskChainer = new TaskChainer(10, 0, 100);

            var averageVal = await taskChainer.ExecuteTasks();

            Console.WriteLine("The average value of the array is " + averageVal);
        }
    }
}

