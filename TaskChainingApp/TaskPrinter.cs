﻿using System;

namespace TaskContinuationApp
{
    internal class TaskPrinter
    {
        internal static void PrintCreatedArray(int[] sourceArray)
        {
            Console.WriteLine("Created array elements:");
            PrintArrayElements(sourceArray);
        }

        internal static void PrintMultipliedArray(int[] sourceArray)
        {
            Console.WriteLine("Multiplied array elements:");
            PrintArrayElements(sourceArray);
        }

        internal static void PrintSortedArray(int[] sourceArray)
        {
            Console.WriteLine("Sorted array elements:");
            PrintArrayElements(sourceArray);
        }

        private static void PrintArrayElements(int[] sourceArray)
        {
            foreach (var item in sourceArray)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine("\n");
        }
    }
}