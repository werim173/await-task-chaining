using System;
using System.Linq;
using System.Threading.Tasks;

namespace TaskContinuationApp
{
    public class TaskChainer
    {
        static Random random = new Random();

        public int ArrSize { get; private set; }

        public int MinNumber { get; private set; }

        public int MaxNumber { get; private set; }

        public TaskChainer(int arrSize, int minNumber, int maxNumber)
        {
            if (arrSize <= 0)
            {
                throw new ArgumentException("Array size must be grater than zero.");
            }

            if (Math.Abs(maxNumber) > 1e4 || Math.Abs(minNumber) > 1e4)
            {
                throw new ArgumentException("Modules of minimun and maximum numbers must not be grater than 10 000.");
            }

            if (maxNumber < minNumber)
            {
                throw new ArgumentException("Maximum number must not be less than minimum number.");
            }

            ArrSize = arrSize;
            MinNumber = minNumber;

            MaxNumber = maxNumber;
        }

        public async Task<double> ExecuteTasks()
        {
            var firstResult = await Task.Run(() =>
            {
                var arr = new int[ArrSize];

                for (int i = 0; i < ArrSize; ++i)
                {
                    arr[i] = random.Next(MinNumber, MaxNumber);
                }

                TaskPrinter.PrintCreatedArray(arr);

                return arr;
            });

            var secondResult = await Task.Run(() => MultiplyIntegersArray(firstResult));
            var thirdResult = await Task.Run(() => SortArrayByAsc(secondResult));
            var forthResult = await Task.Run(() => CalculateAverageValue(thirdResult));

            return forthResult;
        }

        public int[] MultiplyIntegersArray(int[] sourceArray)
        {
            if (sourceArray == null)
            {
                throw new ArgumentNullException(nameof(sourceArray));
            }

            if (sourceArray.Length == 0)
            {
                throw new ArgumentException("Source array must not be empty.");
            }

            var number = random.Next(MinNumber, MaxNumber);

            var result = sourceArray.Select(x => x * number).ToArray();
            TaskPrinter.PrintMultipliedArray(result);

            return result;
        }

        public int[] SortArrayByAsc(int[] sourceArray)
        {
            if (sourceArray == null)
            {
                throw new ArgumentNullException(nameof(sourceArray));
            }

            if (sourceArray.Length == 0)
            {
                throw new ArgumentException("Source array must not be empty.");
            }

            var result = sourceArray.OrderBy(x => x).ToArray();
            TaskPrinter.PrintSortedArray(result);

            return result;
        }

        public double CalculateAverageValue(int[] sourceArray)
        {
            if (sourceArray == null)
            {
                throw new ArgumentNullException(nameof(sourceArray));
            }

            if (sourceArray.Length == 0)
            {
                throw new ArgumentException("Source array must not be empty.");
            }

            var value = sourceArray.Average();

            return value;
        }
    }
}
