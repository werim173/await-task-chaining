using NUnit.Framework;
using System;
using System.Collections;
using System.Linq;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using System.Threading.Tasks;
using TaskContinuationApp;

namespace TaskContinuationTests
{

    [TestFixture]
    public class TaskChainerTests
    {
        static TaskChainer testChainer;
        static int[] sourceArr = Enumerable.Range(0, 10).ToArray();

        [SetUp]
        public void SetUp()
        {
            testChainer = new TaskChainer(10, 10, 100);
        }

        [TestCase(0)]
        [TestCase(-5)]
        public void TaskChainer_Constructor_NonPositiveArraySize_ArgumentException(int arrSize)
        {
            Assert.Throws<ArgumentException>(() => new TaskChainer(arrSize, 10, 100));
        }

        [TestCase(-10000, 100000)]
        [TestCase(-10001, 1)]
        [TestCase(1, 10001)]
        [TestCase(5, 0)]
        public void TaskChainer_Constructor_WrongMinMaxNumbers_ArgumentException(int minNumber, int maxNumber)
        {
            Assert.Throws<ArgumentException>(() => new TaskChainer(10, minNumber, maxNumber));
        }

        [TestCase(10, 10, 100)]
        [TestCase(10, 10, 10)]
        public void TaskChainer_Constructor_CreateCorrectInstance(int arrSize, int minNumber, int maxNumber)
        {
            var chainer = new TaskChainer(arrSize, minNumber, maxNumber);

            var expectedValues = new[] { arrSize, minNumber, maxNumber };
            var actualValues = new[] { chainer.ArrSize, chainer.MinNumber, chainer.MaxNumber };

            Assert.AreEqual(expectedValues, actualValues);
        }

        [Test]
        public async Task TaskChainer_ExecuteTasks_CorrectExecution()
        {
            var result = await testChainer.ExecuteTasks();

            Assert.Pass();
        }

        [Test]
        public void TaskChainer_MultiplyIntegersArray_ArrayIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => testChainer.MultiplyIntegersArray(null));
        }

        [Test]
        public void TaskChainer_MultiplyIntegersArray_ArrayIsEmpty_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => testChainer.MultiplyIntegersArray(Array.Empty<int>()));
        }

        [Test]
        public void TaskChainer_MultiplyIntegersArray_CorrectArray()
        {
            var actualArr = testChainer.MultiplyIntegersArray(sourceArr);

            Assert.AreNotEqual(sourceArr, actualArr);
        }

        [Test]
        public void TaskChainer_SortArrayByAsc_ArrayIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => testChainer.SortArrayByAsc(null));
        }

        [Test]
        public void TaskChainer_SortArrayByAsc_ArrayIsEmpty_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => testChainer.SortArrayByAsc(Array.Empty<int>()));
        }

        [Test]
        public void TaskChainer_SortArrayByAsc_CorrectArray()
        {
            var unsortedArr = sourceArr.OrderByDescending(x => x).ToArray();

            var actualArr = testChainer.SortArrayByAsc(unsortedArr);

            Assert.AreEqual(sourceArr, actualArr);
        }

        [Test]
        public void TaskChainer_CalculateAverageValue_ArrayIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => testChainer.CalculateAverageValue(null));
        }

        [Test]
        public void TaskChainer_CalculateAverageValue_ArrayIsEmpty_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => testChainer.CalculateAverageValue(Array.Empty<int>()));
        }

        [Test]
        public void TaskChainer_CalculateAverageValue_CorrectArray()
        {
            var expectedVal = sourceArr.Average();

            var actualVal = testChainer.CalculateAverageValue(sourceArr);

            Assert.AreEqual(expectedVal, actualVal);
        }
    }
}